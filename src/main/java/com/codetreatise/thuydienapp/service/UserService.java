package com.codetreatise.thuydienapp.service;

import com.codetreatise.thuydienapp.bean.User;
import com.codetreatise.thuydienapp.generic.GenericService;

public interface UserService extends GenericService<User> {

	boolean authenticate(String email, String password);
	
	User findByEmail(String email);
	User getActiveUser();
}
